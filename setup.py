from setuptools import setup
from setuptools import find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='ai_babysitter',
      version='0.0.0',
      description='AI Babysitter, for no one ever',
      long_description=long_description,
      author='Audrey Beard',
      author_email='audrey.s.beard@gmail.com',
      packages=find_packages(),
      install_requires=['torch',
                        'tensorboard',
                        ],
      url='https://gitlab.com/AudreyBeard/ai_babysitter',
      changelog={'0.0.0': 'This is a dumb project',
                 }
      )
