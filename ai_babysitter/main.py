import torch
import tensorboard_logger
tensorboard_logger.configure('.')


class AIBabysitter(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.is_smiling = bool(torch.randint(2, (1, )))
        print("Welcome to our AI Babysitter app!")
        print("Check my progress with `tensorboard --logdir .`")
        self.iteration = 0
        self.do_sitting()

    def forward(self, is_smiling):
        self.is_smiling = is_smiling
        tensorboard_logger.log_value("Smiling :)", int(is_smiling), self.iteration)
        return True

    def do_sitting(self):
        if self.is_smiling:
            status, good_bad = "smiling", "good. Let's keep it that way!"
        else:
            status, good_bad = "crying", "bad. Let's change that!"
        print("It looks like your baby is {}, that's {}".format(status, good_bad))

        self(True)
        if status == "bad":
            print("There, your baby is smiling now!")

        self.iteration += 1

    def query(self):
        status = "smiling" if self.is_smiling else "crying"
        q = "Is the baby still {}?\n>> ".format(status)
        self.is_smiling = input(q).startswith("ye")
        return q

    def check_if_parents_back(self):
        back = input("Are you back yet?\n>> ")
        back = back.startswith("ye")
        return back

    def leave(self):
        print("omg good, I'm a shitty techneoliberal proxy for parenting")


if __name__ == "__main__":
    babysitter = AIBabysitter()
    while not babysitter.check_if_parents_back():
        babysitter.query()
        babysitter.do_sitting()
    babysitter.leave()
